#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is dedicated to the public domain under the CC0 license.

"""
Bienvenue sur le code de GenevaBot! Un petit bot post covid19 qui vous indiquera les activités ludiques
à faire à Genève, enjoy !
"""

import logging, sys, requests, time, math

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

bot_token = sys.argv[1]

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

DESIRE, ENVIE_SORTIE, RESTAURANT, QUEL_RESTO, RETOUR_SORTIE, RETOUR_RESTO, STATE_TRANSPORT = range(7)

# Récupération des actions à faire en ce qui concerne le GenevaBot

def start(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Propose moi des sorties', 'Propose moi des restos'],
    ]

    update.message.reply_text(
        'Salut ! Je suis GenevaBot, je suis là pour te proposer différentes activités à Genève ! '
        'Si tu veux arrêter de me parler, envoi simplement /cancel (◕‿◕✿)\n\n'
        'Je peux te conseiller des sorties ou des restos sympa ! Qu est-ce qui t\'intéresserais ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return DESIRE

def desire_sorties(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Musées','Bars','Clubs'],
        ['Retour']
    ]

    user = update.message.from_user
    logger.info("Desire de %s: %s", user.first_name, update.message.text)
    update.message.reply_text(
        'Très bien ! Que veux-tu faire ? '
        'Si tu préfèrais aller manger, tape seulement sur retour !',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return ENVIE_SORTIE

def desire_restos(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Japonais', 'Indien', 'Chinois','Italien','Thaï'],
        ['Retour']
    ]

    user = update.message.from_user
    logger.info("Desire de %s: %s", user.first_name, update.message.text)

    update.message.reply_text(
        'Cool ! Quel genre de resto te ferais plaisir ? '
        'Si tu préfèrais faire une sortie, tape seulement sur retour !',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return RESTAURANT

def sortie_musees(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Retour']
    ]

    user = update.message.from_user
    logger.info("Desire de %s: %s", user.first_name, update.message.text)

    update.message.reply_text(
        'Tu veux te faire un peu de culture ? Je peux te proposer les musées suivants...\n\n '
        'Le musée d\'art et d\'histoires,\n '
        'Le musée d\'ethnographie,\n '
        'Ou bien le musée international de la Croix-Rouge et du croissant-rouge',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return RETOUR_SORTIE

def sortie_bars(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Retour']
    ]

    user = update.message.from_user
    logger.info("Desire des %s: %s", user.first_name, update.message.text)

    update.message.reply_text(
        'J\'espère que tu n\'as pas prévu de conduire après... Je peux te proposer les bars suivants...\n\n '
        'Hors du Temps, La Forge, ou bien La Citadelle \U0001F37B',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return RETOUR_SORTIE

def sortie_clubs(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Retour']
    ]

    user = update.message.from_user
    logger.info("Desire des %s: %s", user.first_name, update.message.text)

    update.message.reply_text(
        'Je connais des endroits sympas pour aller danser ! Je peux te proposer les clubs suivants... \n\n'
        'Mambo club, L\'Usine, ou bien Le Baroque',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return RETOUR_SORTIE

def resto_choix(update: Update, context: CallbackContext) -> chr:
    reply_keyboard = [
        ['Bao Canteen'], ['Molino'], ['Little India'],['Les Marroniers'],['Odéon']
        ]

    user = update.message.from_user
    logger.info("Desire un resto %s: %s", user.first_name, update.message.text),

    update.message.reply_text(
        'Miam ! Ces restaurants devraient t\'intéresser...\n\n'
        '• Bao Canteen\n'
        '• Molino\n'
        '• Little India\n'
        '• Les Marroniers\n'
        '• Odéon',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return QUEL_RESTO

def manger(update: Update, context: CallbackContext) -> str:
    reply_keyboard = [
        ['Retour']
    ]

    user = update.message.from_user
    logger.info("%s desire aller au resto: %s", user.first_name, update.message.text),

    update.message.reply_text(
        'Très bon choix. Voici des informations utiles concernant le restaurant que tu as choisis: \n\n'
        'Le restaurant est noté 4.2 ★ sur Google.\n'
        'Il est ouvert du lundi au dimanche, de 11h30 à 21h30.\n'
        'Tu peux manger sur place ou commander sur Eat.ch \n'
        'Tu trouveras ton restaurant ici:',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    update.message.reply_location('46.1983453', '6.1428947')

    return RETOUR_RESTO

def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info(" %s est partie.", user.first_name)
    update.message.reply_text(
        'Ho tu pars déjà ? Bon, au revoir dans ce cas, et à la prochaine !', reply_markup=ReplyKeyboardRemove()
    )
    update.message.reply_sticker("https://github.com/TelegramBots/book/raw/master/src/docs/sticker-fred.webp")

    return ConversationHandler.END



"""
Cette partie concerne l'implémentation des Transports, le code initial est celui de @Khodl
"""


# Fonctions communes

def appeler_opendata(path):
    url = "https://transport.opendata.ch/v1/" + path
    result = requests.get(url)
    print(url)
    return result.json()

def afficher_arrets(arrets, update):
    for arret in arrets['stations']:
        if arret['id'] is not None:
            update.message.reply_text("/stop" + arret['id'] + " - " + arret['name'])

def calculer_temps(timestamp):
    seconds = timestamp - time.time()
    minutes = seconds/60;
    minutes = math.floor(minutes)

    if minutes < 0:
        return "Trop tard"

    if(minutes < 3):
        return "Presque parti"

    return "{} min".format(minutes)


# Récupération des actions à faire en ce qui concerne les transports

def message_de_bienvenue(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Salut ! Pourrais-tu m'envoyer ta localisation (en texte ou en pièce jointe) ~(˘▾˘~) ?")

    return STATE_TRANSPORT

def rechercher_par_localisation(update: Update, context: CallbackContext) -> None:
    location = update.message.location
    arrets = appeler_opendata("locations?x={}&y={}".format(location.longitude, location.latitude) )
    afficher_arrets(arrets, update)

def rechercher_par_nom(update: Update, context: CallbackContext) -> None:
    arrets = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(arrets, update)


def afficher_horaires(update: Update, context: CallbackContext) -> None:
    arret_id = update.message.text[5:]
    prochains_departs = appeler_opendata("stationboard?id={}&limit=10".format(arret_id))

    reponse = "Voici les prochains départs:\n\n"

    for depart in prochains_departs["stationboard"]:
        stop = depart['stop']
        reponse = reponse + calculer_temps(stop['departureTimestamp']) + ' ' + depart['number'] + " -> " + depart['to'] + "\n"

    reponse = reponse + "\nPour actualiser: /stop{}".format(arret_id)
    update.message.reply_text(reponse)

updater = Updater(bot_token)

# Get the dispatcher to register handlers
dispatcher = updater.dispatcher


# Conversation handler with the states DESIRE, ENVIE_SORTIE, RESTAURANT, QUEL_RESTO, RETOUR_SORTIE and RETOUR_RESTO
conv_handler = ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={
        DESIRE: [
            MessageHandler(Filters.regex('^(Propose moi des sorties)$'), desire_sorties),
            MessageHandler(Filters.regex('^(Propose moi des restos)$'), desire_restos)
        ],
        ENVIE_SORTIE: [
            MessageHandler(Filters.regex('^(Musées)$'), sortie_musees),
            MessageHandler(Filters.regex('^(Bars)$'), sortie_bars),
            MessageHandler(Filters.regex('^(Clubs)$'), sortie_clubs),
            MessageHandler(Filters.regex('^(Retour)$'), start)
        ],
        RESTAURANT: [
            MessageHandler(Filters.regex('^(Japonais|Indien|Chinois|Italien|Thaï)$'), resto_choix),
            MessageHandler(Filters.regex('^(Retour)$'), start)
        ],
        QUEL_RESTO: [
            MessageHandler(Filters.regex('^(Bao Canteen|Molino|Little India|Les Marroniers|Odéon)$'), manger),
        ],
        RETOUR_SORTIE: [
            MessageHandler(Filters.regex('^(Retour)$'), desire_sorties),
        ],
        RETOUR_RESTO: [
            MessageHandler(Filters.regex('^(Retour)$'), resto_choix),
        ],
    },
    fallbacks=[CommandHandler('cancel', cancel)],
)


# Conversation handler de STATE_TRANSPORT
conv_handler_transport = ConversationHandler(
    entry_points=[CommandHandler('transport', message_de_bienvenue)],
    states={
        STATE_TRANSPORT: [
            MessageHandler(Filters.location, rechercher_par_localisation),
            MessageHandler(Filters.command, afficher_horaires),
            MessageHandler(Filters.text, rechercher_par_nom),
        ],
    },
    fallbacks=[CommandHandler('cancel', cancel)],
)

dispatcher.add_handler(conv_handler_transport)
dispatcher.add_handler(conv_handler)

updater.start_polling()
updater.idle()


